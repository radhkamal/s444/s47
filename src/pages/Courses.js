import PropTypes from 'prop-types';
import {Fragment} from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses() {
	console.log(coursesData);

	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		);
	})

	return (
		<Fragment>
			{courses}
		</Fragment>
	);	
}

CourseCard.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}