import {Fragment} from 'react'; // hindi magkaerror pag multiple components ang irereturn
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';

import './App.css';


// function App() {
//   return (
//     <Fragment>
//           <AppNavbar />
//         <Container>
//           <Banner />
//           <Highlights />
//         </Container>
//     </Fragment>
//   );
// }


function App() {
  return (
    <Fragment>
        <AppNavbar />
        <Container>
{/*          <Home/>
          <Courses/>*/}
{/*
          <Register />*/}
          <Login />
        </Container>
    </Fragment>
  );
}

export default App;
